const Books = require('./PromiseProblem')
const logBook = {}
let currentDate = new Date()


function singIn(username) {
    return new Promise((resolve, reject) => {
        if (currentDate.getSeconds() > 30) {
            logActivity(username, 'Sign In - Success')
            return resolve(username)
        } else {
            logActivity(username, 'Sign In - Failure')
            return reject('Authentication Failure')
        }
    })
}


function getBooks(username, books) {
    const integer = Math.floor(Math.random() * 2)
    return new Promise((resolve, reject) => {
        if (integer === 1) {
            logActivity(username,'GetBooks - Success')
            return resolve(books)
        } else {
            logActivity(username,'GetBooks - Failure')
            return reject('Service Error')
        }
    })
}

function logActivity(user, activity) {
    return Promise.resolve(
        logBook[user] = [activity]
    )
}


function displayLogs() {
    console.log(logBook)
}


singIn('Mary').then((user) => {
    return getBooks(user, Books)
}).then(res => {
    console.log(res)
}).catch(err => {
    console.log(err)
}).finally(() => displayLogs())


singIn('Emily').then((user) => {
    return getBooks(user, Books)
}).then(res => {
    console.log(res)
}).catch(err => {
    console.log(err)
}).finally(() => displayLogs())



